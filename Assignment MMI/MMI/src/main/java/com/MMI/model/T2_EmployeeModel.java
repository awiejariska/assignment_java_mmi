package com.MMI.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T2_Employee")
public class T2_EmployeeModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name ="id", length = 11, nullable = false)
	private Long id;
	
	@Column(name="name", length = 100, nullable = false)
	private String name;
	
	@Column(name="birth_date", nullable = false)
	private LocalDate birthDate;
	
	@Column(name="position_id", length = 11, nullable = false)
	private Integer positionId;
	
	@ManyToOne
	@JoinColumn(name="position_id", insertable = false, updatable = false)
	public T1_PositionModel T1_Position;
	
	@Column(name="id_number", length = 20, nullable = false)
	private Integer idNumber;
	
	@Column(name="gender", length = 20, nullable = false)
	private String gender;
	
	@Column(name="is_delete", nullable = false)
	private Boolean isDelete = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	public T1_PositionModel getT1_Position() {
		return T1_Position;
	}

	public void setT1_Position(T1_PositionModel t1_Position) {
		T1_Position = t1_Position;
	}

	public Integer getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(Integer idNumber) {
		this.idNumber = idNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
