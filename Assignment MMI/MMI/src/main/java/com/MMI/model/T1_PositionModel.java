package com.MMI.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T1_Position")
public class T1_PositionModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name ="id", length = 11, nullable = false)
	private Long id;
	
	@Column(name="code", length = 50, nullable = false)
	private String code;
	
	@Column(name="name", length = 100, nullable = false)
	private String name;
	
	@Column(name="is_delete", nullable = false)
	private Boolean isDelete = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
