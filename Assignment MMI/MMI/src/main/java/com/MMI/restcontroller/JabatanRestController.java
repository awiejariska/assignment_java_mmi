package com.MMI.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.MMI.model.T1_PositionModel;
import com.MMI.service.JabatanService;

@RestController
@RequestMapping(path="/api/jabatan", produces="application/json")
@CrossOrigin(origins = "*")
public class JabatanRestController {
	@Autowired
	private JabatanService jabatanS;
	
	@GetMapping("/")
	public ResponseEntity<?>getAllJabatan(){
		return new ResponseEntity<>(jabatanS.getAlljabatan(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveJabatan(@RequestBody T1_PositionModel jabatan){
		return new ResponseEntity<>(jabatanS.save(jabatan), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteJabatan(@PathVariable("id") Long id) {
		try {
			jabatanS.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> EditJabatan(@RequestBody T1_PositionModel jabatan) {
		return new ResponseEntity<>(jabatanS.save(jabatan), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(jabatanS.findById(id), HttpStatus.OK);
	}

}
