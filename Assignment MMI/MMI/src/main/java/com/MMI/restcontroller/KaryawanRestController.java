package com.MMI.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.MMI.model.T2_EmployeeModel;
import com.MMI.service.KaryawanService;

@RestController
@RequestMapping(path="/api/karyawan", produces="application/json")
@CrossOrigin(origins = "*")
public class KaryawanRestController {
	@Autowired
	private KaryawanService karyawanS;
	
	@GetMapping("/")
	public ResponseEntity<?>getAllEmployee(){
		return new ResponseEntity<>(karyawanS.getAllEmployee(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveKaryawan(@RequestBody T2_EmployeeModel karyawan){
		return new ResponseEntity<>(karyawanS.save(karyawan), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteKaryawan(@PathVariable("id") Long id) {
		try {
			karyawanS.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> EditKaryawan(@RequestBody T2_EmployeeModel karyawan) {
		return new ResponseEntity<>(karyawanS.save(karyawan), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(karyawanS.findById(id), HttpStatus.OK);
	}
}
