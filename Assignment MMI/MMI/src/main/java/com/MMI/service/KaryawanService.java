package com.MMI.service;

import java.util.List;
import java.util.Optional;

import com.MMI.model.T2_EmployeeModel;

public interface KaryawanService {
	List<T2_EmployeeModel> getAllEmployee();
	
	void delete(Long id);
	
	T2_EmployeeModel save(T2_EmployeeModel Karyawan);
	
	Optional<T2_EmployeeModel> findById(Long id);
}
