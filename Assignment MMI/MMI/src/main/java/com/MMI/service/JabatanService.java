package com.MMI.service;

import java.util.List;
import java.util.Optional;

import com.MMI.model.T1_PositionModel;

public interface JabatanService {
	List<T1_PositionModel> getAlljabatan();
	
	void delete(Long id);
	
	T1_PositionModel save(T1_PositionModel Jabatan);
	
	Optional<T1_PositionModel> findById(Long id);

}
