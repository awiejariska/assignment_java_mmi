package com.MMI.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.MMI.model.T2_EmployeeModel;
import com.MMI.repo.KaryawanRepo;
import com.MMI.service.KaryawanService;

@Service
public class KaryawanServiceImpl implements KaryawanService  {
	@Autowired
	private KaryawanRepo karyawanRepo;
	
	@Override
	public List<T2_EmployeeModel> getAllEmployee() {
		// TODO Auto-generated method stub
		return karyawanRepo.getAllEmployee();
	}
	
	@Override
	public T2_EmployeeModel save(T2_EmployeeModel Karyawan) {
		// TODO Auto-generated method stub
		return karyawanRepo.save(Karyawan);
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		karyawanRepo.deleteById(id);
	}
	
	@Override
	public Optional<T2_EmployeeModel> findById(Long id) {
		// TODO Auto-generated method stub
		return karyawanRepo.findById(id);
	}
}
