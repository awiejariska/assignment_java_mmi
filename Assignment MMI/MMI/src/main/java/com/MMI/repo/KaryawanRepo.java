package com.MMI.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.MMI.model.T2_EmployeeModel;

@Repository
public interface KaryawanRepo extends JpaRepository<T2_EmployeeModel, Long>{
	@Query("select e "
			+ "from T2_EmployeeModel e where isDelete = false")
	List<T2_EmployeeModel> getAllEmployee();
}
