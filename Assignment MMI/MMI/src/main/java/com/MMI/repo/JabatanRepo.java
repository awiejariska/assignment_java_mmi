package com.MMI.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.MMI.model.T1_PositionModel;

public interface JabatanRepo extends JpaRepository<T1_PositionModel, Long> {
	@Query("select p "
			+ "from T1_PositionModel p where isDelete = false")
	List<T1_PositionModel> getAlljabatan();
}
