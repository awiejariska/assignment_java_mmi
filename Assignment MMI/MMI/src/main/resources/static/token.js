/**
 * 
 */
$(function () {
    $("#exp").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    
});

$('#btnCalendar').click(function(){
	    $("#exp").focus();
})


	//send Email (fahmi)
	var idSend	
	sendAction=(e)=>{
	 $.ajax({
	        url:'/api/biodata/'+ e.value,
	        type: 'get',
	        contentType: 'application/json',
	        success : (respond) =>{
	         	idSend = respond.id
	         	if(respond.expiredToken==null){
		             $('#exp').val('')
	         	} else {
	         		
	        	let c = new Date(respond.expiredToken).toString().split(" ")
	        	if(c[1]=="Jan"){c[1] = "01"}
				else if(c[1]=="Feb"){c[1] = "02"}
				else if(c[1]=="Mar"){c[1] = "03"}
				else if(c[1]=="Apr"){c[1] = "04"}
				else if(c[1]=="May"){c[1] = "05"}
				else if(c[1]=="Jun"){c[1] = "06"}
				else if(c[1]=="Jul"){c[1] = "07"}
				else if(c[1]=="Aug"){c[1] = "08"}
				else if(c[1]=="Sep"){c[1] = "09"}
				else if(c[1]=="Oct"){c[1] = "10"}
				else if(c[1]=="Nov"){c[1] = "11"}
				else if(c[1]=="Dec"){c[1] = "12"};
				let expDate = c[3]+"-"+c[1]+"-"+c[2];
				
	             $('#exp').val(expDate)
	         	}
	            $('#modalSend').modal("show")
	            $("#exp_send").show()
	            $("#exp_loading").hide()
	            $("#exp_error_message").hide();
	        },
	        error:()=>{
	            console.log('Failed to fetch data');
	        }
	    });
   			
	}
	
	$(function(){
		$("#exp_error_message").hide();
		var error_exp = false;
	})
	function check_exp(){
			var c = $('#exp').val().split("-");
			var expTime = c[0]+"-"+c[1]+"-"+c[2];
			console.log(expTime)
			var c = Date().toString().split(" ");
			if(c[1]=="Jan"){c[1] = "01"}
			else if(c[1]=="Feb"){c[1] = "02"}
			else if(c[1]=="Mar"){c[1] = "03"}
			else if(c[1]=="Apr"){c[1] = "04"}
			else if(c[1]=="May"){c[1] = "05"}
			else if(c[1]=="Jun"){c[1] = "06"}
			else if(c[1]=="Jul"){c[1] = "07"}
			else if(c[1]=="Aug"){c[1] = "08"}
			else if(c[1]=="Sep"){c[1] = "09"}
			else if(c[1]=="Oct"){c[1] = "10"}
			else if(c[1]=="Nov"){c[1] = "11"}
			else if(c[1]=="Dec"){c[1] = "12"};
			var nowTime = c[3]+"-"+c[1]+"-"+c[2];
			console.log(nowTime)
			
		
			if(expTime<nowTime){
				$("#exp_error_message").html("Tanggal tidak boleh kurang dari hari ini");
	            $("#exp_error_message").show();
				error_exp = true
			} else {
				$("#exp_error_message").hide();
			}
	}
	

	var error_exp,expToken,dateNow;
	$('#exp').change(function(){
		error_exp = false;
		check_exp();
	})
	
	$('#sendBtn').click(function(){
		error_exp = false;
		check_exp();
		if(error_exp == false) {
		$("#exp_send").hide()
		$("#exp_loading").show()
		let exp = new Date($("#exp").val())
	     $.ajax({
	            url: 'api/biodata/send/'+ idSend +'/'+ exp,
	            type: 'put',
	            contentType: "application/json",
	            success: function (result) {
	                $("#modalSend").modal("hide");
	                Swal.fire({
	                      position: 'top-center',
	                      icon: 'success',
	                      width: 200,
	                      title: ' Data success',
	                      showConfirmButton: false,
	                      timer: 1500
	                    });
	            },
	            error: function () {
	                 Swal.fire({
	                  icon: 'warning',
	                  text: 'gagal mengirim',
	                  showConfirmButton: false,
	                  timer: 1500
	                });
	            }
	        })
		} else {
			return false
		}
	})
	
	

	
